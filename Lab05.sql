
--1

create or replace procedure category_change(vOld in varchar2, vNew in varchar2) as
    ex_no_data exception;
    counting number(2);
    begin
        select count(category) into counting 
        from books 
            where category = vOld;
        if counting < 1 then
            raise ex_no_data;
        else
            update books
            set category = vNew
                where category = vOld;
        end if;
    EXCEPTION
        WHEN ex_no_data THEN
            dbms_output.put_line('Category not found');
    end;

execute category_change('FIT', 'FITNESS');

--2

create or replace procedure add_publisher(vId in number, vName in varchar2, vContact in varchar2, vPhone in varchar2) as
    ex_existingPublisher exception;
    counting number;
begin
    select MAX(pubid) into counting 
        from publisher;
    if vId <= counting then
        raise ex_existingPublisher;
    else
        insert into publisher
            (pubid,publisher.name,contact,phone)
        VALUES
            (vId,vName,vContact,vPhone);
    end if;
EXCEPTION
    WHEN ex_existingPublisher THEN
        dbms_output.put_line('Publisher already exists');
end;

execute add_publisher(6,'LE_PRINT','CHARLES','514-566-2384');

--3

create or replace function search_Author(vISBN in varchar2) 
return varchar2 as
    vFullname varchar2(20);
    vCount number;
    TOO_MANY_AUTHORS exception;
    NO_AUTHORS exception;
begin
    select COUNT(authorid) into vCount
        from author
            inner join bookauthor
            using (authorid)
                where ISBN = vISBN;
    if vCount>1 then
        raise TOO_MANY_AUTHORS;
    elsif vCount<1 then
        raise NO_AUTHORS;
    else
    select fname||' '||lname into vFullname
        from author
            inner join bookauthor
            using (authorid)
                where ISBN = vISBN;
        return vfullname;
    end if;
EXCEPTION
    WHEN TOO_MANY_AUTHORS THEN
        dbms_output.put_line('multiple authors');
    WHEN NO_AUTHORS THEN
        dbms_output.put_line('author does not exist');
end;

declare
    v varchar2(20);
begin
    v := search_Author('0401140733');
    dbms_output.put_line(v);
end;

--4 I don't know how to do this other than to put the TOO_MANY_AUTHORS in a package, which isn't MANY_AUTHORS

create or replace procedure title_Author(vTitle in varchar2) as
vISBN varchar2(20);
MANY_AUTHOR exception;
vTest number;
begin
    select ISBN into vISBN
        from books
            where title = vTitle;
    dbms_output.put_line(vTitle||', '||search_Author(vISBN));
EXCEPTION
    WHEN MANY_AUTHOR THEN
        dbms_output.put_line(vTitle||', '||'Multiple');
end;

execute title_Author('BODYBUILD IN 10 MINUTES A DAY');

--5 Don't know how to verify my answer here

create or replace type tpurchasearray is varray(1) of number;

create or replace function book_purchasers(vISBN in varchar2) 
return tpurchasearray as
Parray tpurchasearray;
vId number;
vCount number;
begin
Parray := tpurchasearray(0);
select customer# into vId
    from customers
        inner join orders
        using (customer#)
            inner join orderitems
            using (order#)
                where ISBN = vISBN and rownum = 1;
select count(customer#) into vId
    from customers
        inner join orders
        using (customer#)
            inner join orderitems
            using (order#)
                where ISBN = vISBN;
Parray(1) := vId;
if vCount > 1 then
    for vindex in 2..vCount
    loop
        Parray.extend();
        select customer# into vId
    from customers
        inner join orders
        using (customer#)
            inner join orderitems
            using (order#)
                where ISBN = vISBN and rownum = vindex;
        Parray(vindex) := vId;
    end loop;
end if;
    return Parray;
end;

--6 Don't know how to verify my answer for #5 so I can't really do #6